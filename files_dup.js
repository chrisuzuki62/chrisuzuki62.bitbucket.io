var files_dup =
[
    [ "Driver_Controller.py", "Driver__Controller_8py.html", [
      [ "Driver_Controller.Driver_Controller", "classDriver__Controller_1_1Driver__Controller.html", "classDriver__Controller_1_1Driver__Controller" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", [
      [ "DRV8847.DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "DRV8847.Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.encoder", "classencoder_1_1encoder.html", "classencoder_1_1encoder" ]
    ] ],
    [ "IMU.py", "IMU_8py.html", [
      [ "IMU.IMU", "classIMU_1_1IMU.html", "classIMU_1_1IMU" ]
    ] ],
    [ "Lab_0.py", "Lab__0_8py.html", null ],
    [ "Lab_1.py", "Lab__1_8py.html", "Lab__1_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "Panel_Driver.py", "Panel__Driver_8py.html", [
      [ "Panel_Driver.Panel_Driver", "classPanel__Driver_1_1Panel__Driver.html", "classPanel__Driver_1_1Panel__Driver" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_collect.py", "task__collect_8py.html", "task__collect_8py" ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.task_controller", "classtask__controller_1_1task__controller.html", "classtask__controller_1_1task__controller" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.task_encoder", "classtask__encoder_1_1task__encoder.html", "classtask__encoder_1_1task__encoder" ]
    ] ],
    [ "task_IMU.py", "task__IMU_8py.html", "task__IMU_8py" ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.task_motor", "classtask__motor_1_1task__motor.html", "classtask__motor_1_1task__motor" ]
    ] ],
    [ "task_panel.py", "task__panel_8py.html", "task__panel_8py" ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "task_user_tp.py", "task__user__tp_8py.html", "task__user__tp_8py" ]
];