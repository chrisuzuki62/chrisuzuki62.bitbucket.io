var main_8py =
[
    [ "ADC_share", "main_8py.html#a9296bd12c8fba3866c2fc6f04e1e76a5", null ],
    [ "ADC_vel_share", "main_8py.html#a847dec77f9daa4cb5779647bbd6c9a41", null ],
    [ "ang_vel_share", "main_8py.html#ab354efa66cb34cc51a26a4002c46ee46", null ],
    [ "breaks", "main_8py.html#abc40cd90ad70b849bcd0342b076f5b63", null ],
    [ "collect_share", "main_8py.html#af4a928043f4be797e99e79a7cbcb087a", null ],
    [ "euler_angle_share", "main_8py.html#a05ccc2e0047e37805b171ffd46c49f84", null ],
    [ "motor_share", "main_8py.html#a87940df9d58c2d3dd215e3589c7dbb3e", null ],
    [ "state_control_share", "main_8py.html#ab64a321392c41fc080fb9afc8aab828e", null ],
    [ "Stop", "main_8py.html#a2d01364fa9e5dbd03f6b268c835dba9c", null ],
    [ "task1", "main_8py.html#af4b8f4290f8d32e70654f6deb864787f", null ],
    [ "task2", "main_8py.html#a99d189b8f9eb3784cf7d4e5c3241b562", null ],
    [ "task3", "main_8py.html#a120516a5d42c212c898d4616d266077a", null ],
    [ "task4", "main_8py.html#ac0ee3c5b5359f76becfe27534e82085f", null ],
    [ "task5", "main_8py.html#a11ec9e490c402876467b9fdadecc93e8", null ],
    [ "task6", "main_8py.html#a4ff32207e9201d99684e38af7bee4d5d", null ]
];