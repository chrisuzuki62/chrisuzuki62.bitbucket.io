var classtask__panel_1_1task__panel =
[
    [ "__init__", "classtask__panel_1_1task__panel.html#a231205537e779667f783ba92b4fb27cf", null ],
    [ "run", "classtask__panel_1_1task__panel.html#a3e21e395e3f747b558b609305d5fa0f2", null ],
    [ "transition_to", "classtask__panel_1_1task__panel.html#aa5157ba4090a41a92c7b7500fc8feb3f", null ],
    [ "ADC_share", "classtask__panel_1_1task__panel.html#a2a19f0ba8e59f588153a24194ec48e01", null ],
    [ "ADC_vel_share", "classtask__panel_1_1task__panel.html#aa1190679d8989ca6809badd48eb2fb5a", null ],
    [ "last_time", "classtask__panel_1_1task__panel.html#abb2efcaaa6f8ac10ee36c7c8e35d3fcb", null ],
    [ "next_time", "classtask__panel_1_1task__panel.html#aea031f220ccfaaaec166764a4be0d044", null ],
    [ "Panel", "classtask__panel_1_1task__panel.html#ac836eae925e6279cf6694015266201e8", null ],
    [ "period", "classtask__panel_1_1task__panel.html#af0bc55002191c8d0721a9d36dca1858e", null ],
    [ "state", "classtask__panel_1_1task__panel.html#ad3150efb3b8b52e58708dbc1c12e683d", null ],
    [ "x_val_last", "classtask__panel_1_1task__panel.html#a9fd0a70790388372b85cce9f22a8ab7c", null ],
    [ "y_val_last", "classtask__panel_1_1task__panel.html#a0bf35a77fc6bd2dd3212c4a4f9b00ca5", null ]
];