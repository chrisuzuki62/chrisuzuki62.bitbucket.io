var classlab3__task__motor_1_1task__motor =
[
    [ "__init__", "classlab3__task__motor_1_1task__motor.html#a22e26c4da15b621c471604c3d49b9f57", null ],
    [ "run", "classlab3__task__motor_1_1task__motor.html#adfe4221a2ffd48821e4c266a8544c4f6", null ],
    [ "fault_flag", "classlab3__task__motor_1_1task__motor.html#a4669e36d55ad903996f26c62984046ab", null ],
    [ "motor1_share", "classlab3__task__motor_1_1task__motor.html#a03264e029efa7e9565c5496a3a9d7cd0", null ],
    [ "motor2_share", "classlab3__task__motor_1_1task__motor.html#a559f6d63ad57e0ea69201f1062c2e712", null ],
    [ "motor_1", "classlab3__task__motor_1_1task__motor.html#a428a7a34793f1d6225165c2bb00b1ef6", null ],
    [ "motor_2", "classlab3__task__motor_1_1task__motor.html#ae7e7f647619fe45ee3e11361c3f1aa74", null ],
    [ "motor_drv", "classlab3__task__motor_1_1task__motor.html#aa7d4a46cf1cb133a0bd20850f52aba30", null ],
    [ "next_time", "classlab3__task__motor_1_1task__motor.html#aead2b6782becd2231392ce7823d08598", null ],
    [ "period", "classlab3__task__motor_1_1task__motor.html#a9cc9d1656ade2931299e0ae51a42494d", null ]
];