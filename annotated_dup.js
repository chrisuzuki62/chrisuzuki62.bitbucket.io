var annotated_dup =
[
    [ "Driver_Controller", null, [
      [ "Driver_Controller", "classDriver__Controller_1_1Driver__Controller.html", "classDriver__Controller_1_1Driver__Controller" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "encoder", "classencoder_1_1encoder.html", "classencoder_1_1encoder" ]
    ] ],
    [ "IMU", null, [
      [ "IMU", "classIMU_1_1IMU.html", "classIMU_1_1IMU" ]
    ] ],
    [ "Panel_Driver", null, [
      [ "Panel_Driver", "classPanel__Driver_1_1Panel__Driver.html", "classPanel__Driver_1_1Panel__Driver" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_collect", null, [
      [ "task_collect", "classtask__collect_1_1task__collect.html", "classtask__collect_1_1task__collect" ]
    ] ],
    [ "task_controller", null, [
      [ "task_controller", "classtask__controller_1_1task__controller.html", "classtask__controller_1_1task__controller" ]
    ] ],
    [ "task_encoder", null, [
      [ "task_encoder", "classtask__encoder_1_1task__encoder.html", "classtask__encoder_1_1task__encoder" ]
    ] ],
    [ "task_IMU", null, [
      [ "task_IMU", "classtask__IMU_1_1task__IMU.html", "classtask__IMU_1_1task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "task_motor", "classtask__motor_1_1task__motor.html", "classtask__motor_1_1task__motor" ]
    ] ],
    [ "task_panel", null, [
      [ "task_panel", "classtask__panel_1_1task__panel.html", "classtask__panel_1_1task__panel" ]
    ] ],
    [ "task_user", null, [
      [ "task_user", "classtask__user_1_1task__user.html", "classtask__user_1_1task__user" ]
    ] ],
    [ "task_user_tp", null, [
      [ "task_user", "classtask__user__tp_1_1task__user.html", "classtask__user__tp_1_1task__user" ]
    ] ]
];